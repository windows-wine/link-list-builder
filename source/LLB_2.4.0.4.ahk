;================================================================
;	DIRECTIVES
;================================================================
#NoEnv
#SingleInstance, Force
ListLines, Off
SetControlDelay, -1
SetWinDelay, -1
SetBatchLines, -1
ListLines, Off
StringCaseSense, On
SetFormat, Integer, D
DetectHiddenWindows, On
CoordMode, Mouse, Relative
updates()
#include lib\MI.ahk
;DebugBIF()
;================================================================
; COMPILER DIRECTIVES FOR AHK 1.1+
;================================================================
;@Ahk2Exe-Obey bits, = %A_PtrSize% * 8
;@Ahk2Exe-Obey xbit, = "%A_PtrSize%" > 4 ? "x64" : "x86"
;@Ahk2Exe-Obey type, = "%A_IsUnicode%" ? "Unicode" : "ANSI"
;@Ahk2Exe-Obey xtype, = "%A_IsUnicode%" ? "W" : "A"
;@Ahk2Exe-Let version = 2.4.0.4
;@Ahk2Exe-AddResource %A_ScriptDir%\res\skin.bmp, 100
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\LLB.ico, 159
;@Ahk2Exe-SetFileVersion %U_version%
;@Ahk2Exe-SetProductVersion 2.4.0.0
;@Ahk2Exe-SetProductName Link List Builder
;@Ahk2Exe-SetInternalName LLB.ahk
;@Ahk2Exe-SetOrigFilename LLB.exe
;@Ahk2Exe-SetDescription Link List Builder is an address book for e-mails and URLs
;@Ahk2Exe-SetCompanyName Drugwash Hobby Programming
;@Ahk2Exe-SetCopyright Copyright � Drugwash`, June 2015 - Aug 2023
;@Ahk2Exe-SetLegalTrademarks Released under the terms of the GNU General Public License
;@Ahk2Exe-Set Comments, Compiled in %A_AhkVersion% %U_type% %U_bits%bit.
;@Ahk2Exe-SetMainIcon %A_ScriptDir%\res\LLB.ico
;@Ahk2Exe-UpdateManifest 0, Drugwash.HobbyProgramming.LinkListBuilder, %U_version%
;================================================================
;	IDENTIFICATION
;================================================================
author = Drugwash								; main script author
authormail = drugwash@mail.com					; author e-mail address
authorsite = https://gitlab.com/windows-wine	; author official web site
apphome = %authorsite%/link-list-builder		; application homepage
comment = Keeps track of URLs and e-mails, builds lists
appname = Link List Builder						; application name
version = 2.4.0.4								; version number
releaseD = August 22, 2023						; release date
releaseT = public beta							; release type (internal/public alpha/beta/final)
iconlocal = %A_ScriptDir%\res\LLB.ico			; icon for uncompiled script
debug=%1%
;================================================================
OnExit, cleanup	; Moved it here to make sure it will clean up even during tests when not fully loaded
inifile := A_ScriptDir "\LLB_preferences.ini"
avdir := A_ScriptDir "\avatars"
dbdir := A_ScriptDir "\db"
apptmp := A_Temp "\LLB"
appnameS := "LLB"		; Short name for InputBox titles
iRq := "http://www.gravatar.com/avatar/"
iExt := "jpg"
tbvis=0			; toolbar 2 visibility (initialize to zero for new/empty database)
hInst := DllCall("GetModuleHandle" AW, Ptr, NULL)
Global cmdid
TBB=23|New database
	,28|Open database
	,21|Import database
	,1||0x1	;============ separator
	,36|Save database
	,33|Save database as...|0x18
	,1||0x1	;============ separator
	,47|Use avatars|0x12
	,44|Update avatar(s)|0x18
	,1||0x1	;============ separator
	,26|New entry|0x18
	,14|Edit entry||0
	,13|Remove entry|0x18|0
	, 51|Apply||0				; for hidden use 0x8
	, 50|Cancel||0				; for hidden use 0x8
	,1||0x1	;============ separator
	,32|Settings
	,1|About
	,1||0x1	;============ separator
	,18|Exit
TBA=37|Select all
	,39|Select none
	,38|Invert selection
	,1||0x1	;============ separator
	,20|Use HTML formatting|0x12
	,27|Display links as nicknames|0x12|0
	,19|Force pingbacks|0x12|0
	,1||0x1	;============ separator
	,40|Separator: comma|0x16
	,41|Separator: new line|0x16
	,42|Separator: comma + new line|0x16
	,43|Separator: space|0x16
	,1||0x1	;============ separator
	,17|Mailto|0x18	;|0x8
	,30|Open link(s)|0x18		; use 0 to disable button
	,1||0x1	;============ separator
	,7|Copy nick(s)|0x18
	,5|Copy e-mail(s)|0x18
	,6|Copy link(s)|0x18
midb := "1|6|33 34,1|9|46 45 44,1|11|26 25 24,1|13|53 52 13,2|14|16 15 17,2|17|10 4 7,2|18|8 2 5
	,2|19|9 3 6"
tmidb := "48,32,1,0,49,18"
dbcol := "Nick,e-mail,URL,Alias,Real name,Birthdate,Sex,Private e-mail,Private phone,Home address,Home IM,Work URL,Work e-mail,Work phone,Work address,Work IM,Alt. URLs,Alt. e-mails,Alt. phones,Alt- addresses,Alt. IMs,Notes"
dbread := "5,1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22"
lvcols=1
Loop, Parse, dbcol, CSV
	lvcols++
Loop, Parse, dbread, CSV
	g%A_Index% := A_LoopField
;=== ICONS STUFF ===
GetIcon(0, 1)				; Initialize System Image List handle (internally, not needed explicitly)
if A_IsCompiled
	{
	FileCreateDir, %apptmp%
	FileInstall, res\icons.dll , %apptmp%\icons.dll, 1
	}
icons1 := (A_IsCompiled ? apptmp : "res") "\icons.dll"
; we need to read how many icon groups are in the library and use that var below
IGCnt := GetIGC(icons1)		; get icon group count from the library
hILt1 := ILC_Create(IGCnt, 1, "16x16", "M32")	; ILC_COLOR32 ILC_MASK
hILt2 := ILC_Create(IGCnt, 1, "24x24", "M32")	; ILC_COLOR32 ILC_MASK
hILt3 := ILC_Create(IGCnt, 1, "32x32", "M32")	; ILC_COLOR32 ILC_MASK
Loop, %IGCnt%
	{
	IL_Add(hILt1, icons1, A_Index)
	IL_Add(hILt2, icons1, A_Index)
	IL_Add(hILt3, icons1, A_Index)
	}
;=== TOOLBAR SKIN STUFF === (reused from RPTE)
skinres=100							; skin resource number in exe
IniRead, pref_isize, %inifile%, Preferences, IconSize, 1
IniRead, skinSrc, %appini%, Preferences, ExternalSkin, 0
IniRead, skinPath, %appini%, Preferences, ExternalSkinPath, %A_Space%
IniRead, skinsz, %inifile%, Preferences, SkinW, 2
IniRead, tSkin, %inifile%, Preferences, SkinType, 0	; start with random skin if not configured
if tSkin
	IniRead, cSkin, %inifile%, Preferences, SkinColor, 23
else Random, cSkin, 1, 32				; use random skin color as default or by choice
defskin := A_IsCompiled ? A_ScriptFullPath : "res\skin.bmp"
skin := skinSrc && skinPath ? skinPath : defskin
GetBmpInfo(skin "|" skinres, sbw, sbh, sbpp)	; Find bitmap width beforehand for calculations
if skinsz not between 2 and %sbw%		; Reset skin band width to minimum
	skinsz=2							; if exceeds total strip width
if (cSkin > sbw//skinsz)					; Reset current skin index to 1
	Random, cSkin, 1, sbw//skinsz		; if exceeds total band count in strip
hSkin := GetBmp(cSkin, skinsz, skin, skinres, 0)	; handle to skin bitmap (returns ImageList handle in 'skin')
skincount := ILC_Count(skin)				; number of skin images
skincolor := GetPixelColor(hSkin, 0, 1)
menucolor := SwColor(skincolor)			; menu background color
;=== REGISTERED BROWSERS ENUMERATION ===
if rbidx := EnumBrowsers(blist, bdef)
;=== MENU STUFF ===
	Loop, Parse, blist, `n, `r
		{
		i := A_Index
		StringSplit, b, A_LoopField, |
		bpath%i% := b3 "\" b2
		hIcon%i% := GetIcon(bpath%i%, pref_isize="3" ? 2 : 3)	; 2=large icon handle, 3=small icon handle
		Menu, brlist1, Add, %b1% , browse
		Menu, brlist2, Add, %b1% , browse
		Menu, brlist3, Add, %b1% , browse
		}
Menu, menu1_6, Add, Backup without avatars, m1_6a
Menu, menu1_6, Add, Backup with avatars, m1_6b
Menu, menu1_9, Add, Update missing avatar(s), m1_9a
Menu, menu1_9, Add, Update checked avatars, m1_9b
Menu, menu1_9, Add, Update all avatars, m1_9c
Menu, menu1_11, Add, Add new entry, m1_11a
Menu, menu1_11, Add, Add new e-mail entry, m1_11b
Menu, menu1_11, Add, Add new link entry, m1_11c
Menu, menu1_13, Add, Remove selected entry, m1_13a
Menu, menu1_13, Add, Remove checked entries, m1_13b
Menu, menu1_13, Add, Remove all entries, m1_13c
Menu, menu2_14, Add, Mailto selected, m2_14a
Menu, menu2_14, Add, Mailto checked, m2_14b
Menu, menu2_14, Add, Mailto all, m2_14c
Menu, menu2_15, Add, Open selected link, m2_15a
if rbidx
	Menu, menu2_15, Add, Open selected link in�, :brlist1
Menu, menu2_15, Add
Menu, menu2_15, Add, Open checked link(s), m2_15b
if rbidx > 1
	Menu, menu2_15, Add, Open checked link(s) in�, :brlist2
Menu, menu2_15, Add
Menu, menu2_15, Add, Open all links, m2_15c
if rbidx > 2
	Menu, menu2_15, Add, Open all links in�, :brlist3
Menu, menu2_17, Add, Copy selected nick, m2_17a
Menu, menu2_17, Add, Copy checked nick(s), m2_17b
Menu, menu2_17, Add, Copy all nicks, m2_17c
Menu, menu2_18, Add, Copy selected e-mail, m2_18a
Menu, menu2_18, Add, Copy checked e-mail(s), m2_18b
Menu, menu2_18, Add, Copy all e-mails, m2_18c
Menu, menu2_19, Add, Copy selected link, m2_19a
Menu, menu2_19, Add, Copy checked link(s), m2_19b
Menu, menu2_19, Add, Copy all links, m2_19c
clrMenus := "6,9,11,13"
clrMenus2 := "14,15,17,18,19"
Loop, Parse, clrMenus, CSV
	Menu, menu1_%A_LoopField%, Color, %menucolor%
Loop, Parse, clrMenus2, CSV
	Menu, menu2_%A_LoopField%, Color, %menucolor%
Menu, Tray, UseErrorLevel
Menu, Tray, Icon, % A_IsCompiled ? "" : iconlocal
Menu, Tray, Tip, %appname% %version%
Menu, Tray, NoStandard
Menu, Tray, Add, AlwaysOnTop, AOT
Menu, Tray, Default, AlwaysOnTop
Menu, Tray, Add, Settings, m1_17
Menu, Tray, Add, About..., m1_18
Menu, Tray, Add
Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, m1_20
Menu, Tray, Click, 1
Menu, Tray, Color, %menucolor%

IniRead, mode, %inifile%, Preferences, ListMode, 0
IniRead, useav, %inifile%, Preferences, UseAvatars, 1
IniRead, lvlv, %inifile%, Preferences, LargeViewSize, 96
IniRead, lvsv, %inifile%, Preferences, SmallViewSize, 32
IniRead, lt, %inifile%, Preferences, Separator, 1
IniRead, hf, %inifile%, Preferences, FormatHTML, 1
IniRead, wn, %inifile%, Preferences, WithNicks, 1
IniRead, ff, %inifile%, Preferences, ForceFollow, 1
IniRead, opendelay, %inifile%, Preferences, LinkListOpenDelay, 2.5
IniRead, sbreset, %inifile%, Preferences, SBResetDelay, 7
opendelay := opendelay > 25 ? 2.5 : opendelay	; contain delay to a reasonable maximum of 25sec.
gosub initsep		; initialize separator

;=== REBAR & TOOLBAR STUFF ===
Gui, +LastFound -OwnDialogs +Resize +MinSize610x465
hMain := WinExist()
Gui, 1:Show, w1000 h480 Hide
if !hRB1 := RB_Create(hMain, "pT s0xA620")	; 0x2740
	msgbox, Cannot create Rebar!
hTB1 := TB_Create("1", "0|0|0|0", "0x896D", "0x89")
TB_SetIL(hTB1, "I0|" hILt%pref_isize%, 5)	; add D,H,P0 lists (Disabled, Hot, Pressed) space-separated
hTB2 := TB_Create("1", "0|0|0|0", "0x896D", "0x89")
TB_SetIL(hTB2, "I0|" hILt%pref_isize%, 5)	; add D,H,P0 lists (Disabled, Hot, Pressed) space-separated
Loop, Parse, TBB, CSV
	{
	i1 := i2 := i3 := i4 := ""
	StringSplit, i, A_LoopField, |, %A_Space%
	i3 := i3!="" ? i3 : 0x10
	i4 := i4!="" ? i4 : 0x4
	TB_AddBtn(hTB1, "", A_Index, i4, i3, i1-1, 0)
;	TB_BtnSet(hTB1, A_Index, "t" i2)				; don't do it here coz it breaks b2w !!!
	}
Loop, Parse, TBA, CSV
	{
	i1 := i2 := i3 := i4 := ""
	StringSplit, i, A_LoopField, |, %A_Space%
	i3 := i3!="" ? i3 : 0x10
	i4 := i4!="" ? i4 : 0x4
	TB_AddBtn(hTB2, "", A_Index, i4, i3, i1-1, 0)
;	TB_BtnSet(hTB2, A_Index, "t" i2)				; don't do it here coz it breaks b2w !!!
	}
gosub tbrbset
tsy := h+4
Loop, Parse, TBB, CSV
	{
	i2 := ""
	StringSplit, i, A_LoopField, |, %A_Space%
	TB_BtnSet(hTB1, A_Index, "t" i2)
	}
Loop, Parse, TBA, CSV
	{
	i2 := ""
	StringSplit, i, A_LoopField, |, %A_Space%
	TB_BtnSet(hTB2, A_Index, "t" i2)
	}

TB_BtnSet(hTB1, 8, "s" 4+useav)		; state 'enabled & (un)checked' Use avatars
TB_BtnSet(hTB2, 5, "s" 4+hf)			; state 'checked' Use HTML formatting
TB_BtnSet(hTB2, 6, "s" (hf ? 4+wn : 0))	; state 'enabled' Use nicknames
TB_BtnSet(hTB2, 7, "s" (hf ? 4+ff : 0))	; state 'enabled' Force pingbacks
TB_BtnSet(hTB2, 8+lt, "s5")			; state 'checked' Separator selection
;=== MENU STUFF ===
if !hm2_15 := MI_GetMenuHandle("menu2_15")
	msgbox, No hm2_15!
else MI_SetMenuStyle(hm2_15, 0x04000000)
if rbidx
	Loop, %rbidx%
	{
	if !addicon(hm2_15, 3*A_Index-2, hIcon%bdef%)
		msgbox, addicon() error %ErrorLevel% for hm2_15 - %A_Index%
	if !hBrlist%A_Index% := DllCall("GetSubMenu", Ptr, hm2_15, "Int", 3*A_Index-2)
		msgbox, No hBrlist%A_Index%
	else
		{
		i := A_Index
		MI_SetMenuStyle(hBrlist%i%, 0x04000000)
		if rbidx
			Loop, %rbidx%
				if !addicon(hBrlist%i%, A_Index, hIcon%A_Index%)
					msgbox, addicon() error %ErrorLevel% for hBrlist%i% - %A_Index%
		}
	}
Loop, Parse, midb, CSV
	{
	StringSplit, mc, A_LoopField, |
	StringSplit, ii, mc3, %A_Space%
	if !hm%mc1%_%mc2%
		{
		if hm%mc1%_%mc2% := MI_GetMenuHandle("menu" mc1 "_" mc2)
			MI_SetMenuStyle(hm%mc1%_%mc2%, 0x04000000)
		else continue
		}
	Loop, %ii0%
		{
		hIM%mc1%_%mc2%_%A_Index% := ILC_GetIcon(hILt%pref_isize%, ii%A_Index%)
		addicon(hm%mc1%_%mc2%, A_Index, hIM%mc1%_%mc2%_%A_Index%)
		}
	}

if hmtray := MI_GetMenuHandle("Tray")
	{
	if OSTest("Vista", "L")	; check for anything lower than Vista
		OnMessage(0x404, "tmshow")
	MI_SetMenuStyle(hmtray, 0x04000000)
	Loop, Parse, tmidb, CSV
		{
		hIT%A_Index% := ILC_GetIcon(hILt%pref_isize%, A_LoopField)
		addicon(hmtray, A_Index, hIT%A_Index%)
		}
	}
;=== GUI STUFF ===
StringSplit, i, dbcol, `,
lvcol=
Loop, Parse, dbread, CSV
	lvcol .= i%A_LoopField% "|"
lvcol .= "dummy"
sepSpc := "4,7,11,16,21"
Gui, Font, s8 w400, Tahoma
Gui, Add, Text, x2 y%tsy% w80 h16 +0x200 +Right Section, Nickname:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Public e-mail:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Public URL:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, URL alias:
Gui, Add, Text, xp y+2 w80 hp +0x200 +Right, Real name:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Birthdate:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Sex:
Gui, Add, Text, xp y+2 w80 hp +0x200 +Right, Private e-mail:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Private phone:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Home address:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Home I.M.:
Gui, Add, Text, xp y+2 w80 hp +0x200 +Right, Work URL:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Work e-mail:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Work phone:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Work address:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Work I.M.:
Gui, Add, Text, xp y+2 w80 hp +0x200 +Right, Alt. URLs:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Alt. e-mails:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Alt. phones:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Alt. addresses:
Gui, Add, Text, xp y+0 w80 hp +0x200 +Right, Alt. I.Ms.:
Gui, Add, Text, xp y+2 w80 hp +0x200 +Right, Notes:
Gui, Add, Edit, x+2 ys+1 w200 h15 -Multi +ReadOnly -E0x200 ve1,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve2,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve3,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve4,
Gui, Add, Edit, xp y+2 w200 hp -Multi +ReadOnly -E0x200 ve5,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve6,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve7,
Gui, Add, Edit, xp y+2 w200 hp -Multi +ReadOnly -E0x200 ve8,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve9,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve10,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve11,
Gui, Add, Edit, xp y+2 w200 hp -Multi +ReadOnly -E0x200 ve12,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve13,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve14,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve15,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve16,
Gui, Add, Edit, xp y+2 w200 hp -Multi +ReadOnly -E0x200 ve17,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve18,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve19,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve20,
Gui, Add, Edit, xp y+0 w200 hp -Multi +ReadOnly -E0x200 ve21,
Gui, Add, Edit, xp y+2 w200 hp -Multi +ReadOnly -E0x200 ve22,
Loop, Parse, sepSpc, CSV
	{
	i := tsy+A_LoopField*16
	Gui, Add, Text, x0 y%i% w286 h2 +0x10 vinfoSep%A_Index%,
	}
Gui, Add, ListView, x286 y%tsy% w346 h320 -0x4000000 LV0x14000 -Multi Checked Grid Sort AltSubmit vLV1 gsel
	, %lvcol%
Gui, Add, StatusBar,, Ready
SB_SetParts(156, 65, 65, 24)	; operation|count|marked|lock|DB path
Gui, Show, Hide
GuiControlGet, sb, Pos, msctls_statusbar321
; Generated using SmartGuiXP Creator mod 4.3.29.7
;=== OTHER INITIALIZATIONS ===
IniRead, maxdownerr, %inifile%, Preferences, AvMaxDownErr, 2
IniRead, downdelay, %inifile%, Preferences, AvDownReDelay, 5
IniRead, erravfile, %inifile%, Preferences, AvErrFile, avatars\err.png

Loop, %erravfile%
	erravfile := A_LoopFileLongPath
IfNotExist, %avdir%
	FileCreateDir, %avdir%
IfNotExist, %dbdir%
	FileCreateDir, %dbdir%
IfNotExist, %erravfile%
	{
	FileInstall, avatars\err.png, %avdir%\err.png
	erravfile := avdir "\err.png"
	}
cap := avdir "\0." iExt
IfNotExist, %cap%
	{
	iLnk := iRq "0." iExt "?s=200&r=x&d=mm"	; retrieved image size can be 1-2048px, we use 200px
	isz := UrlDownloadToVar(img, iLnk, "", "")
	FileCreate(cap, img, 0, isz)
	}
gosub buildAvIL
pToken := Gdip_Startup()

IniRead, cdbidx, %inifile%, Databases, currDB, %A_Space%
IniRead, dbfile, %inifile%, Databases, %cdbidx%, %dbdir%
IniRead, dbshow, %inifile%, Preferences, VisCol, 11000000000000000000000
gosub fixview
gosub opendb
tbvis := LV_GetCount() ? TRUE : FALSE
GuiControl, % (mode ? "+Icon" : "+Report"), LV1
OnMessage(0x4E, "notif")				; WM_NOTIFY
OnMessage(0x111, "cmnd")				; WM_COMMAND
Gui, Show, Center w640 h452, %appname%
loaded=1					; switch to avoid autoselection of item in ListView on startup
changes=0					; initialize autosave trigger
me=0						; initialize marked entries counter
Return
;================================================================
;				END OF AUTOEXEC SECTION
;================================================================
buildAvIL:
hIL := ILC_Create(1, 1, lvlv, "M32")	; LV Large view
hILs := ILC_Create(1, 1, lvsv, "M32")	; LV small view
o1 := LV_SetImageList(hIL, 0)			; large view
o2 := LV_SetImageList(hILs, 1)			; small view
IL_Add(hILs, cap, 0xFF00FF, 1)
IL_Add(hIL, cap, 0xFF00FF, 1)
IL_Add(hILs, erravfile, 0xFF00FF, 1)
IL_Add(hIL, erravfile, 0xFF00FF, 1)
return
;================================================================
tbrbset:
	btnsz := 8*(pref_isize+1), vtbpad := 0x4, htbpad := 0x8
	TB_Set(hTB1, "s0x996D b" btnsz | (btnsz<<16) " d" vtbpad | (htbpad<<16))
	TB_Set(hTB2, "s0x996D b" btnsz | (btnsz<<16) " d" vtbpad | (htbpad<<16))
	b1w := TB_Get(hTB1, "sW")				; initial toolbar width
	b1h := TB_Get(hTB1, "bH")				; initial toolbar height
	b2w := TB_Get(hTB2, "sW")				; initial toolbar width
	b2h := TB_Get(hTB2, "bH")				; initial toolbar height
	if !band1
		if !band1 := RB_Add(hRB1, hTB1, "Toolbar", "", 0, b1w, b1h, 0x6C4, 0)	; use 0x4C4 or 0xCC4
			msgbox, Cannot add Toolbar to Rebar!
	if !band2
		if !band2 := RB_Add(hRB1, hTB2, "Toolbar", "", 0, b2w, b2h, 0x6C4, 0)	; use 0x4C4 or 0xCC4
			msgbox, Cannot add Toolbar to Rebar!

	TB_Size(hTB1)
	TB_Size(hTB2)
	r := RB_Get(hRB1, "b" band1)			; get band1 borders
	StringSplit, m, r, %A_Space%
	i := w9x ? 100+5*(4-pref_isize) : 0	; The rebar control misbehaves on 9x so it needs hacks :-(
	RB_Set(hRB1, band1, "ic" b1w+i "|" b1h "|" btnsz*3 "|" b1h "|" btnsz*3 "|" b1w+i)
	RB_Set(hRB1, band2, "ic" b2w+i "|" b2h "|" btnsz*3 "|" b2h "|" btnsz*3 "|" b2w+i)
	RB_Size(hRB1)
	if hBack
		if !DllCall("DeleteObject", Ptr, hBack)
			msgbox, Can't delete old skin bitmap hBack=%hBack%
	h := RB_GetBand(hRB1, band1, "hr")
	w1 := RB_GetBand(hRB1, band1, "w"), w2 := RB_GetBand(hRB1, band2, "w")
	w := w1 > w2 ? w1 : w2
	hBack := ResizeBmp(hSkin, w, h)
	RB_Set(hRB1, band1, "bk" hBack)
	RB_Set(hRB1, band2, "bk" hBack)
return
;================================================================
reload:
Reload
;================================================================
m1_20:
GuiClose:
ExitApp
;================================================================
cleanup:
;================================================================
if changes
	gosub m1_5
Gui, Submit
IniWrite, %pref_isize%, %inifile%, Preferences, IconSize
;IniWrite, %skinSrc%, %appini%, Preferences, ExternalSkin
;IniWrite, %skinPath%, %appini%, Preferences, ExternalSkinPath
;IniWrite, %skinsz%, %inifile%, Preferences, SkinW
IniWrite, %tSkin%, %inifile%, Preferences, SkinType
IniWrite, %cSkin%, %inifile%, Preferences, SkinColor
IniWrite, %mode%, %inifile%, Preferences, ListMode
IniWrite, %dbshow%, %inifile%, Preferences, VisCol
IniWrite, %useav%, %inifile%, Preferences, UseAvatars
IniWrite, %lvlv%, %inifile%, Preferences, LargeViewSize
IniWrite, %lvsv%, %inifile%, Preferences, SmallViewSize
IniWrite, %lt%, %inifile%, Preferences, Separator
IniWrite, %hf%, %inifile%, Preferences, FormatHTML
IniWrite, %wn%, %inifile%, Preferences, WithNicks
IniWrite, %ff%, %inifile%, Preferences, ForceFollow
IniWrite, %opendelay%, %inifile%, Preferences, LinkListOpenDelay
IniWrite, %sbreset%, %inifile%, Preferences, SBResetDelay
IniWrite, %maxdownerr%, %inifile%, Preferences, AvMaxDownErr
IniWrite, %downdelay%, %inifile%, Preferences, AvDownReDelay
IniWrite, %erravfile%, %inifile%, Preferences, AvErrFile
if cdbidx
	{
	IniWrite, %cdbidx%, %inifile%, Databases, currDB
	IniWrite, %dbfile%, %inifile%, Databases, %cdbidx%
	}
if hRB1
	RB_Cleanup(hRB1)	; no need for TB_Cleanup(hTB1), the Rebar will delete all child windows in its bands
					; Also no need for RB_Destroy() since AHK will destroy the Rebar itself
Loop, %rbidx%
	DllCall("DestroyIcon", Ptr, hIcon%A_Index%)
Loop, Parse, midb, CSV
	{
	StringSplit, mc, A_LoopField, |
	StringSplit, ii, mc2, %A_Space%
	Loop, %ii0%
		DllCall("DestroyIcon", Ptr, hIM%mc1%_%mc2%_%A_Index%)
	}
Loop, Parse, tmidb, CSV
	DllCall("DestroyIcon", Ptr, hIT%A_Index%)
DllCall("DeleteObject", Ptr, hSetClr)
DllCall("DeleteObject", Ptr, hSkin)
DllCall("DeleteObject", Ptr, hBack)	; must fix RB_Cleanup()
IL_Destroy(skin)
IL_Destroy(hILt1)
IL_Destroy(hILt2)
IL_Destroy(hILt3)
IL_Destroy(hIL)
IL_Destroy(hILs)
Gdip_Shutdown(pToken)
FileRemoveDir, %A_Temp%\LLB, 1
ExitApp
;================================================================
GuiContextMenu:	; TOGGLE LISTVIEW MODE
;================================================================
c := A_GuiControl
if c=LV1
	{
	mode := !mode
	GuiControl, % (mode ? "+Icon" : "+Report"), LV1
	LV_ModifyCol(1, "AutoHdr Sort Logical")
	}
return
;================================================================
GuiSize:			; RESIZING
;================================================================
if (ny=ony)
	{
	RB_SizeBand(hRB1, band1, 0)
;	RB_Show(hRB1, band1, TRUE)
	RB_Show(hRB1, band2, tbvis)
	}
AGuiW := A_GuiWidth, AGuiH := A_GuiHeight
ny := RB_Get(hRB1, "h")+4
lvH := AGuiH-ny-sbH
GuiControl, % "1:" (ny != ony ? "MoveDraw" : "Move"), LV1, % "y" ny " h" lvH " w" AGuiW-288
if (ny != ony)
	{
	Loop, Parse, sepSpc, CSV
		GuiControl, 1:MoveDraw, infoSep%A_Index%, % "y" ny+A_LoopField*16
	Loop, 22
		{
		GuiControl, 1:MoveDraw, Static%A_Index%, % "y" ny+16*(A_Index-1)
		GuiControl, 1:MoveDraw, Edit%A_Index%, % "y" ny+16*(A_Index-1)+1
		}
	}
LV_ModifyCol(1, "AutoHdr Sort Logical")
if (ny=ony)
	return
ony := ny
return
;================================================================
sel:				; LISTVIEW SELECTION
;================================================================
if !loaded
	return
e := A_GuiEvent
if e not in Normal,K,I,F,DoubleClick
	return
x := A_EventInfo
if x not between 1 and % LV_GetCount()
	return
row := x
if e=DoubleClick
	{
	isMarked := LV_GetNext(row-1, "C")
	LV_Modify(row, (isMarked=row ? "-Check" : "Check"))
	me := (isMarked=row) ? --me : ++me
	SB_SetText(me " marked", 3)
	}
Loop, Parse, dbread, CSV
	LV_GetText(e%A_LoopField%, row, A_Index)
Loop, Parse, dbread, CSV
	GuiControl,, e%A_Index%, % e%A_Index%
TB_BtnSet(hTB1, 12, "s4")	; state 'enabled' Edit entry
TB_BtnSet(hTB1, 13, "s4")	; state 'enabled' Remove entry
return
;================================================================
AOT:			; ALWAYS ON TOP
;================================================================
aot := !aot
Gui, % (aot ? "+AlwaysOnTop" : "-AlwaysOnTop")
Menu, Tray, ToggleCheck, AlwaysOnTop
return
;================================================================
action1:			; TOOLBAR1 ACTION TBIdx
;================================================================
goto m1_%TBIdx%
return
;================================================================
action2:			; TOOLBAR2 ACTION TB2Idx
;================================================================
goto m2_%TB2Idx%
return
;================================================================
menus1:			; SHOW TOOLBAR1 MENU
;================================================================
MI_ShowMenu(hm1_%cmdid%, 0)
return
;================================================================
menus2:			; SHOW TOOLBAR2 MENU
;================================================================
MI_ShowMenu(hm2_%cmdid%, 0)
return
;================================================================
;========================== TOOLBAR 2 ACTIONS ========================
;================================================================
m2_1:			; SELECT ALL
;================================================================
Loop, % LV_GetCount()
	LV_Modify(A_Index, "Check")
SB_SetText(LV_GetCount() " marked", 3)
goto setFoc
;================================================================
m2_2:			; SELECT NONE
;================================================================
Loop, % LV_GetCount()
	LV_Modify(A_Index, "-Check")
SB_SetText("0 marked", 3)
goto setFoc
;================================================================
m2_3:			; INVERT SELECTION
;================================================================
me=0
Loop, % LV_GetCount()
	{
	i := LV_GetNext(A_Index-1, "C")
	LV_Modify(A_Index, (i=A_Index ? "-" : "") "Check")
	if (i != A_Index)
		me++
	}
SB_SetText(me " marked", 3)

setFoc:
s := LV_GetNext()
LV_Modify(s, "-Select -Focus")
LV_Modify(s, "Select Focus Vis")
return
;================================================================
m2_5:			; USE HTML FORMATTING
;================================================================
hf := TB_BtnState(hTB2, 5, "c") ? 1 : 0
TB_BtnSet(hTB2, 6, "s" (hf ? 4+wn : 0))	; state 'enabled' Use nicknames
TB_BtnSet(hTB2, 7, "s" (hf ? 4+ff : 0))	; state 'enabled' Force pingbacks
goto initsep
return
;================================================================
m2_6:			; USE NICKNAMES IN LIST
;================================================================
wn := TB_BtnState(hTB2, 6, "c") ? 1 : 0
return
;================================================================
m2_7:			; FORCE PINGBACKS
;================================================================
ff := TB_BtnState(hTB2, 7, "c") ? 1 : 0
return
;================================================================
m2_9:			; SEPARATOR SELECTION
m2_10:
m2_11:
m2_12:
;================================================================
lt := SubStr(A_ThisLabel, 4)-8
initsep:
nl := hf ? "</br>" : "`n"
sep := lt=1 ? ", " : lt=2 ? nl : lt=3 ? "," nl : A_Space
return
;================================================================
m2_14:			; MAILTO SELECTED/CHECKED/ALL
m2_14a:
m2_14b:
m2_14c:
LV_GetText(mail, row, 3)	; e-mail is in column 3
Run, mailto:%mail%,, UseErrorLevel
;msgbox, Mailto command %A_ThisLabel% not implemented.
return
;================================================================
m2_15:
i=1
xx := bdef		; use default browser position in browsers list
goto openlinkdef

m2_15a:
m2_15b:
m2_15c:
i := Asc(SubStr(A_ThisLabel, 0, 1))-96	; convert a/b/c into 1/2/3
xx := bdef		; use default browser position in browsers list
goto openlinkdef
;================================================================
browse:			; OPEN LINK
;================================================================
i := SubStr(A_ThisMenu, 0,1)	; 1=selected, 2=checked, 3=all
xx := A_ThisMenuItemPos
openlinkdef:
if i=1
	{
	if !row
		{
		MsgBox, 0x42030, %appname%, Please select an entry in the list below!
		return
		}
	LV_GetText(link, row, 4)	; link is in column 4
	if debug
		msgbox, % bpath%xx% " " link
	else Run, % bpath%xx% " " link,, UseErrorLevel
	}
else if i=2
	{
	r=0
	links=
	Loop
		{
		if !r := LV_GetNext(r, "C")
			break
		LV_GetText(link, r, 4)
		links .= link " "
if !debug
		Run, % bpath%xx% " " link,, UseErrorLevel
		Sleep, opendelay*1000
		}
	if !links
		{
		MsgBox, 0x42030, %appname%, Please mark at least one entry in the list below!
		return
		}
if debug
	msgbox, % bpath%xx% " " links
	}
else if i=3
	{
	MsgBox, 0x42134, %appname% warning,
		(LTrim
		You chose to open ALL links in the browser!
		This can dramatically slow down your computer
		or even freeze it when resources are scarce.
		There will also be a short delay of a few seconds
		before opening each link, to avoid browser choke.

		Are you really sure you want to do this?
		)
	IfMsgBox No
		return
	Loop, % LV_GetCount()
		{
		LV_GetText(link, A_Index, 4)
if !debug
		Run, % bpath%xx% " " link,, UseErrorLevel
		Sleep, opendelay*1000
		}
	}
return
;================================================================
m2_17:			; COPY SELECTED NICK / E-MAIL / LINK
m2_17a:
m2_18:
m2_18a:
m2_19:
m2_19a:
xx := SubStr(A_ThisLabel, 4, 2)-16
;================================================================
if row
	{
	LV_GetText(d1, row, 2)	; nick
	LV_GetText(d2, row, 3)	; e-mail
	LV_GetText(d3, row, 4)	; url
	LV_GetText(d4, row, 5)	; alias
	LV_GetText(d5, row, 1)	; name
	}
else return
if debug
	msgbox, % "Copy selected " (xx=1 ? "Nick" : xx=2 ? "e-mail" : "link") "  --> " d%xx%
else Clipboard := d%xx%
SB_SetText("Selected " (xx=1 ? "nick" : xx=2 ? "e-mail" : "link") " copied to the clipboard", 5)
SetTimer, resetsb, % -1000*sbreset
return
;================================================================
m2_17b:			; COPY CHECKED NICKS / E-MAILS / LINKS
m2_18b:
m2_19b:
;================================================================
xx := SubStr(A_ThisLabel, 4, 2)-16
list=
idx=0
r=0
Loop
	{
	if !r := LV_GetNext(r, "C")
		break
	idx++
	LV_GetText(d1, r, 2)	; nick
	LV_GetText(d%xx%, r, xx+1)
	LV_GetText(d4, r, 5)	; alias
	gosub st%xx%
	list .= stg
	}
if !list
	return
StringTrimRight, list, list, % StrLen(sep2)
if debug
	goto dbgList
Clipboard := list
SB_SetText("Checked " (xx=1 ? "nicks" : xx=2 ? "e-mails" : "links") " were copied to the clipboard", 5)
SetTimer, resetsb, % -1000*sbreset
return
;================================================================
m2_17c:			; COPY ALL NICKS / E-MAILS / LINKS
m2_18c:
m2_19c:
;================================================================
xx := SubStr(A_ThisLabel, 4, 2)-16
list=
Loop, % LV_GetCount()
	{
	idx := A_Index
	LV_GetText(d1, A_Index, 2)			; nick
	if (d1="")
		{
		LV_GetText(d1, A_Index, 5)		; alias
		if (d1="")
			LV_GetText(d1, A_Index, 4)	; url
	LV_GetText(d%xx%, A_Index, xx+1)
	LV_GetText(d4, A_Index, 5)
		}
	gosub st%xx%
	list .= stg
	}
if !list
	return
StringTrimRight, list, list, % StrLen(sep2)
if debug
	goto dbgList
Clipboard := list
SB_SetText("All " (xx=1 ? "nicks" : xx=2 ? "e-mails" : "links") " were copied to the clipboard", 5)
SetTimer, resetsb, % -1000*sbreset
return
;================================================================
st1:
sep2 := sep
stg := d1 sep
return
;================================================================
st2:
if !hf
	{
	sep2 := sep
	stg := d2 sep
	return
	}
sep2 := "`; "
stg := wn ? """" d1 """ <" d2 ">" sep2 : d2 sep2
return
;================================================================
st3:
sep2 := sep
if !hf
	{
	stg := d3 sep
	return
	}
urltxt := !wn ? "link " idx : d4
stg := !wn ? d4 : d1
k := ff ? "dofollow" : "nofollow"
stg := "<a href=""" d3 """ title=""" urltxt """ target=""_blank"" rel=""" k """>" stg "</a>" sep
return
;================================================================
dbgList:
FileDelete, list%xx%.txt
FileAppend, %list%, list%xx%.txt
Run, Notepad list%xx%.txt
return
;================================================================
;========================== TOOLBAR 1 ACTIONS ========================
;================================================================
m1_1:			; NEW DATABASE
;================================================================
MsgBox, 0x42024, %appname% question, Save current database before using the new one?
IfMsgBox, Yes
	gosub m1_6b		; save with avatars
; ask for database name
InputBox, ndb, %appnameS% DB name, Type a name for the new database:,, 250, 120
if (ErrorLevel or ndb="")
	return
; ask for new password
newDbPass:
InputBox, userin, %appnameS% password, Set password for database:, HIDE, 200, 120
if ErrorLevel		; User canceled, no DB is created
	return
if (userin="")
	{
	MsgBox, 0x42024, %appname% question,
		(LTrim
		Leaving the password field blank
		will save the database in plain text.
		This can lead to privacy breach.

		Are you sure you don't want to
		set a password for this database?
		)
	IfMsgBox, No
		goto newDbPass
	}
LV_Delete()
tbvis := FALSE	; hide toolbar 2 when list is empty
RB_Show(hRB1, band2, tbvis)
dbfile := dbdir "\" ndb (userin ? ".edb" : ".db")
; should check if names/paths clash
pass := userin
gosub listdb
SB_SetIcon(pass ? icons1 : "", 12, 4)
return
;================================================================
m1_2:			; OPEN DATABASE
;================================================================
FileSelectFile, i, 3, %dbfile%, Select a LLB database to use:, LLB databases (*.edb; *.db)
if (!i OR ErrorLevel)
	return
idb := i
MsgBox, 0x42024, %appname% question, Save current database before using the new one?
IfMsgBox, Yes
	gosub m1_6b		; save with avatars
dbfile := idb
;================================================================
opendb:
IfNotExist, %dbfile%
	return
if InStr(FileExist(dbfile), "D")	; return if path is a folder
	return
FileRead, strg, *c %dbfile%
Loop, %dbfile%
	if A_LoopFileExt=db
		{
		if A_IsUnicode
			MB2WC(strg, strg2), strg := strg2
		goto plaintxtdb
		}
; ask for password
InputBox, userin, %appnameS% password, Enter database password:, HIDE, 200, 120
if (ErrorLevel or userin="" or !cpu(strg, userin))	; return if canceled, no password or no match
	{
	dbfile := dbdir		; Take out current DB filename to avoid any accidental overwriting
	return			; with a blank new database
	}
pass := userin
;================================================================
plaintxtdb:
loaded=0
LV_Delete()
GuiControl, -Redraw, LV1
Loop, Parse, strg, `n, `r
	{
	i=%A_LoopField%
	if !i
		continue
	Loop, %lvcols%
		e%A_Index% := ""
	StringSplit, e, i, %A_Tab%
	e%g1% := e%g1% ? e%g1% : e%g2%		; if there's no real name, use the Nickname instead
	r := LV_Add("", e%g1%)
	Loop, Parse, dbread, CSV
		LV_Modify(r, "Col" A_Index, e%A_LoopField%)
	LV_Modify(r, "Col" lvcols, "0")
	}
gosub m1_9a		; go get avatars
gosub fixview
GuiControl, +Redraw, LV1
gosub listdb
SB_SetText(LV_GetCount() " entries", 2)
SB_SetIcon(pass ? icons1 : "", 12, 4)
loaded=1
return
;================================================================
m1_3:			; IMPORT DATABASE
;================================================================
FileSelectFile, i, 3, %dbfile%, Select a LLB database to import:, LLB databases (*.edb; *.db)
if (!i OR ErrorLevel OR !FileExist(i))
	return
strg=
Loop, % LV_GetCount()
	{
	idx := A_Index
	Loop, Parse, dbread, CSV
		LV_GetText(d%A_LooField%, idx, A_Index)
	Loop, Parse, dbread, CSV
		strg .= d%A_Index% A_Tab
	StringTrimRight, strg, strg, 1
	strg .= "`n"
	}
FileRead, idb, %i%
Loop, %i%
	if A_LoopFileExt=db
		{
		if A_IsUnicode
			MB2WC(idb, strg2), idb := strg2
		goto plaintxtimp
		}
; ask for password
InputBox, userin, %appnameS% password, Enter database password:, HIDE, 200, 120
if (ErrorLevel or userin="")
	return
; return if no match
if !cpu(idb, userin)
	return
pass := pass ? pass : userin
;================================================================
plaintxtimp:
loaded=0
GuiControl, -Redraw, LV1
Loop, Parse, idb, `n, `r
	{
	i=%A_LoopField%
	if (!i OR InStr(strg, i))				; If blank entry OR entries are identical
		continue							; go to next entry
	Loop, %lvcols%
		e%A_Index% := ""
	StringSplit, e, i, %A_Tab%
	e%g1% := e%g1% ? e%g1% : e%g2%		; if there's no real name, use the Nickname instead
	if !InStr(strg, e%g3% A_Tab e%g4% A_Tab)	; If new e-mail and new link are not in old database,
		{
		r := LV_Add("")	; add new entry
		Loop, Parse, dbread, CSV
			LV_Modify(r, "Col" A_Index, e%A_LoopField%)
		LV_Modify(r, "Col" lvcols, "0")
		}
	else if useimported					; else find existing entry
		Loop, % LV_GetCount()			; and update it with the new values for Nick and Alias
			{
			LV_GetText(d2, A_Index, g3)
			LV_GetText(d3, A_Index, g4)
			if (d2=e%g3% && d3=e%g4%)
				{
				i := A_Index
				Loop, Parse, dbread, CSV
					LV_Modify(i, "Col" A_Index, e%A_LoopField%)
				break
				}
			}
	}
gosub m1_9a		; update all missing avatars at once
gosub fixview
GuiControl, +Redraw, LV1
SB_SetText(LV_GetCount() " entries", 2)
SB_SetIcon(pass ? icons1 : "", 12, 4)
loaded=1
changes++
return
;================================================================
fixview:
Loop, Parse, dbshow
	LV_ModifyCol(A_Index, (A_LoopField ? "AutoHdr" : 0))
tbvis := LV_GetCount() ? TRUE : FALSE
RB_Show(hRB1, band2, tbvis)
return
;================================================================
m1_6b:			; BACKUP WITH AVATARS
ba=1
;================================================================
m1_6:			; BACKUP WITHOUT AVATARS
m1_6a:
;================================================================
SplitPath, dbfile,, olddb
FileSelectFile, i, S26, %dbfile%, Select path and filename for the backup:, LLB databases (*.edb; *.db)
if (!i OR ErrorLevel)
	goto sor
dbfile := i
SplitPath, dbfile,, dd, de, df
if !de
	dbfile := dd "\" df ".edb"
;================================================================
m1_5:			; SAVE DATABASE
;================================================================
if (!dbfile OR dbfile=dbdir)
	goto m1_6
strg=
Loop, % LV_GetCount()
	{
	idx := A_Index
	Loop, Parse, dbread, CSV
		LV_GetText(d%A_LoopField%, idx, A_Index)
	Loop, %lvcols%
		strg .= d%A_Index% A_Tab
	StringTrimRight, strg, strg, 1
	strg .= "`n"
	}
; if no password defined, ask for one
if (pass="")
	{
	InputBox, userin, %appnameS% password, Enter database password:, HIDE, 200, 120
	if ErrorLevel				; User canceled saving
		return
	pass := userin
	if (userin="")				; User did not set a password
		{
		SplitPath, dbfile,, dd, de, df
		dbfile := dd "\" df ".db"
		IfExist, %dbfile%
			FileDelete, %dbfile%
		FileAppend, %strg%, %dbfile%
		goto saveNoPass
		}
	}
; encrypt string before saving
s := StrLen(strg)*2	;**(A_IsUnicode=TRUE)
if A_IsUnicode
	{
	r := VarSetCapacity(buf2, s+1, 0)
	DllCall("RtlMoveMemory", Ptr, &buf2, Ptr, &strg, "UInt", s)
	}
else MB2WC(strg, buf2)
if !nsz := Crypto_RC4(&buf2, s, pass, TRUE)
	{
	MsgBox, 0x42010, %appname% error, Database encryption failed!`nFile not saved.
	return
	}
else
	{
	VarSetCapacity(buf, (sz := nsz+40), 0)
	DllCall("RtlMoveMemory", Ptr, &buf, Ptr, &buf2, "UInt", sz)
	ucrypt := CryptIt(userin, "SHA", FALSE)
	DllCall("RtlMoveMemory", Ptr, &buf+nsz, Ptr, &ucrypt, "UInt", 40)
	}
VarSetCapacity(buf2, 0)
IfExist, %dbfile%
	FileDelete, %dbfile%
FileCreate(dbfile, buf, 0, sz)
saveNoPass:
gosub listdb
changes=0
if !ba
	return
SplitPath, dbfile,, dbpath
dbpath .= "\avatars"
IfNotExist, %dbpath%
	FileCreateDir, %dbpath%
if dbpath=%olddb%\avatars
	return
Loop, %olddb%\avatars\*.*, 0, 0
	FileCopy, %A_LoopFileLongPath%, %dbpath%, 1
sor:		; save option reset (no 'with avatars')
ba=
return
;================================================================
listdb:
;================================================================
Loop
	{
	i := A_Index
	IniRead, d, %inifile%, Databases, %A_Index%, %A_Space%
	if !d
		break
	if (d=dbfile)
		return
	}
cdbidx := i
IniWrite, %cdbidx%, %inifile%, Databases, currDB
IniWrite, %dbfile%, %inifile%, Databases, %cdbidx%
return
;================================================================
m1_8:			; USE AVATARS
;================================================================
useav := TB_BtnState(hTB1, 8, "c")
if !useav
	Loop, % LV_GetCount()
		LV_Modify(A_Index, "Icon999")
else Loop, % LV_GetCount()
	{
	LV_GetText(i, A_Index, lvcols)
	LV_Modify(A_Index, "Icon" i)
	}
return
;================================================================
m1_9c:			; UPDATE ALL AVATARS (FORCED)
;================================================================
fu := TRUE
goto m1_9a
;================================================================
m1_9:			; UPDATE SELECTED AVATAR
;================================================================
IfNotExist, %avdir%
	FileCreateDir, %avdir%
r := row, fu := TRUE
gosub getAV
SB_SetText("Ready")
SB_SetText(dbfile, 5)
fu=
return
;================================================================
m1_9a:			; UPDATE MISSING AVATAR(S)
;================================================================
IfNotExist, %avdir%
	FileCreateDir, %avdir%
Loop, % LV_GetCount()
	{
	r := A_Index
	gosub getAV
	}
SB_SetText("Ready")
SB_SetText(dbfile, 5)
fu=
return
;================================================================
m1_9b:			; UPDATE CHECKED AVATARS
;================================================================
IfNotExist, %avdir%
	FileCreateDir, %avdir%
fu := TRUE
r=0
Loop
	{
	if !r := LV_GetNext(r, "C")
		break
	gosub getAV
	}
SB_SetText("Ready")
SB_SetText(dbfile, 5)
fu=
return
;================================================================
getAV:
;================================================================
LV_GetText(em, r, 3)			; Get e-mail address
LV_GetText(existAv, r, lvcols)	; Check if avatar entry already exists, to avoid IL overload
email = %em%				; get rid of stray spaces
StringLower, email, email			; convert to all lowercase
mHash := CryptIt(email, "MD5", FALSE, TRUE)
if mHash=0					; If e-mail is blank
	{
	avix=1					; set 'shadowman' avatar
	goto setListIcon			; and skip all the fuss below
	}
StringLower, mHash, mHash		; Convert to all lowercase
cap := avdir "\" mHash "." iExt
if (!FileExist(cap) OR fu)
	{
	downerr=0
	iLnk := iRq mHash "." iExt "?s=200&r=x&d=mm"	; retrieved image size can be 1-2048px
downav:
	SB_SetText("Downloading avatar #" r "- " mHash " �", 5)
	isz := UrlDownloadToVar(img, iLnk, "", "")
	if isz						; Don't create zero-sized files on web error
		{
		IfExist, %cap%
			FileDelete, %cap%
		FileCreate(cap, img, 0, isz)
		existAv := existAv < 3 ? 0 : existAv	; if previously blank or errored, add as new
		}
	else
		{
		downerr++
		SB_SetText("Download error! Retry #" downerr)
		if (downerr <= maxdownerr)
			{
			Sleep, downdelay*1000
			goto downav
			}
		avix=2					; If download error, set 'error' avatar
		goto setListIcon		; and skip all the fuss below
		}
	}
SB_SetText("Adding avatar #" r "�")
StringLeft, cap1, cap, % InStr(cap, ".", FALSE, 0)
cap1 .= "png"
; use 'existAv' to modify IL rather than append
if (avix := IL_Replace(hILs, cap, existAv, 0xFFFF00FF))<0	; if current JPG file won't work, try PNG extension
	{
	FileCopy, %cap%, %cap1%, % fu ? TRUE : FALSE
	if (avix := IL_Replace(hILs, cap1, existAv, 0xFFFF00FF))<0
		avix=0
	}
if avix<1
	outputdebug, step 1a i=%avix% existAV=%existAV% r=%r% file %cap%
if (avix := IL_Replace(hIL, cap, existAv, 0xFFFF00FF))<0	; if current JPG file won't work, try PNG extension
	{
	if (avix := IL_Replace(hIL, cap1, existAv, 0xFFFF00FF))<0
		avix=0
	}
if avix<1
	outputdebug, step 2a i=%avix% existAV=%existAV% r=%r% file %cap%
;if (!existAv OR avix < 3)
if avix < 3
	avix += 2	; we already have two icons in the ImageList so bump it up twice
setListIcon:
LV_Modify(r, "Icon" (useav ? avix : "9999") " Col" lvcols, avix)
SB_SetText("Added avatar #" r)
return
;================================================================
m1_11b:			; ADD NEW ENTRY WITH E-MAIL
;================================================================
i := clipboard
if !InStr(i, "@")
	{
	MsgBox, 0x42010, %appname% input error,
		(LTrim
		There is no e-mail address in the clipboard.
		Please copy an e-mail address first!
		)
	return
	}
GuiControl,, e2, %i%
GuiControl,, e3,
GuiControl,, e5,
goto addentry
;================================================================
m1_11c:			; ADD NEW ENTRY WITH LINK
;================================================================
i := clipboard
if !InStr(i, "http://")
if !InStr(i, "https://")
	{
	MsgBox, 0x42010, %appname% input error,
		(LTrim
		There is no valid link in the clipboard.
		Please copy a FULL link (incl. http) first!
		)
	return
	}
GuiControl,, e2,
GuiControl,, e3, %i%
GuiControl,, e5,
goto addentry
;================================================================
m1_11:			; ADD NEW ENTRY
m1_11a:
;================================================================
GuiControl,, e2,
GuiControl,, e3,
GuiControl,, e5,
addentry:
GuiControl,, e1,
GuiControl,, e4,
Loop, 16
	GuiControl,, % "e" A_Index+5,
t=1
TB_BtnSet(hTB1, 11, "s0")	; state 'disabled' Add entry
TB_BtnSet(hTB1, 12, "s0")	; state 'disabled' Edit entry
TB_BtnSet(hTB1, 13, "s0")	; state 'disabled' Remove entry
GuiControl, Disable, LV1
TB_BtnSet(hTB1, 14, "s4")	; state 'enabled' Apply
TB_BtnSet(hTB1, 15, "s4")	; state 'enabled' Cancel
Loop, % lvcols-1
	GuiControl, -ReadOnly, e%A_Index%
return
;================================================================
m1_12:			; EDIT ENTRY
;================================================================
t=2
TB_BtnSet(hTB1, 11, "s0")	; state 'disabled' Add entry
TB_BtnSet(hTB1, 12, "s0")	; state 'disabled' Edit entry
TB_BtnSet(hTB1, 13, "s0")	; state 'disabled' Remove entry
GuiControl, Disable, LV1
TB_BtnSet(hTB1, 14, "s4")	; state 'enabled' Apply
TB_BtnSet(hTB1, 15, "s4")	; state 'enabled' Cancel
Loop, % lvcols-1
	GuiControl, -ReadOnly, e%A_Index%
return
;================================================================
m1_13:			; REMOVE ENTRY
m1_13a:
;================================================================
msg := "selected entry"
gosub confdel
if !ok
	return
r := LV_GetCount()
if row between 1 and %r%
	LV_Delete(row)
changes++
return
;================================================================
m1_13b:			; REMOVE CHECKED ENTRIES
;================================================================
msg := "checked entries"
gosub confdel
if !ok
	return
r=0
Loop
	{
	if !r := LV_GetNext(r, "C")
		break
	LV_Delete(r)
	}
changes++
return
;================================================================
m1_13c:			; REMOVE ALL ENTRIES
;================================================================
msg := "all entries"
gosub confdel
if !ok
	return
LV_Delete()
changes++
return
;================================================================
confdel:
;================================================================
ok := FALSE
MsgBox, 0x42031, %appname%,
	(LTrim
	You are about to remove %msg%
	from this database. There is no undo!

	Proceed with deletion?
	)
IfMsgBox OK
	ok := TRUE
return
;================================================================
m1_14:			; APPLY
;================================================================
Gui, Submit, NoHide
if !e5				; If no Real Name was set
	{
	if e1				; if Nickname is set
		e5 := e1		; set Real Name from Nickname
	else
		{
		MsgBox, 0x42030, %appname% warning,
			(LTrim
			There is no Real Name and
			no Nickname for this entry.
			You must set either of them
			for a correct operation.

			Please set one of them now.
			)
		return
		}
	}
if t=1
	{
	row := LV_Add("")
	Loop, %lvcols%
		i := g%A_Index%, LV_Modify(row, "Col" A_Index, e%i%)
	gosub m1_9			; go get avatar for new entry
	}
else
	{
	Loop, %lvcols%
		i := g%A_Index%, LV_Modify(row, "Col" A_Index, e%i%)
;		LV_Modify(row, "Col" A_Index, % "e" g%A_Index%)
	}
changes++
;================================================================
m1_15:			; CANCEL
;================================================================
t=0
TB_BtnSet(hTB1, 11, "s4")	; state 'enabled' Add entry
TB_BtnSet(hTB1, 12, "s4")	; state 'enabled' Edit entry
TB_BtnSet(hTB1, 13, "s4")	; state 'enabled' Remove entry
TB_BtnSet(hTB1, 14, "s0")	; state 'disabled' Apply
TB_BtnSet(hTB1, 15, "s0")	; state 'disabled' Cancel
GuiControl, Enable, LV1
Loop, % lvcols-1
GuiControl, +ReadOnly, e%A_Index%
; match preferences!!!
Loop, Parse, dbshow, CSV
	LV_ModifyCol(A_Index, A_LoopField ? "AutoHdr" : 0)
LV_Modify(row ? row : "1", "-Select")
LV_Modify(row ? row : "1", "Vis Focus Select")
return
;================================================================
m1_17:			; SETTINGS DIALOG
;================================================================
if !oldrow := LV_GetNext()
	oldrow := 1
skinSelW=192		; specify skin selector control width for use below
tscbw := skinSelW//32
Gui, 2:+Owner1
Gui, 2:Add, Text, x10 y16 w140 h21 +0x200 +Right, Toolbar icon size:
Gui, 2:Add, Text, x10 y38 w140 h21 +0x200 +Right, Small view avatar size:
Gui, 2:Add, Text, x10 y60 w140 h21 +0x200 +Right, Large view avatar size:
Gui, 2:Add, DropDownList, x152 y16 w50 h74 R5 AltSubmit vswv1, 16||24|32
Gui, 2:Add, DropDownList, x152 y38 w50 h21 R5 vswv2, 16|24|32||48
Gui, 2:Add, DropDownList, x152 y60 w50 h21 R4 vswv3, 32|48|64|80|96||128|160|200
Gui, 2:Add, Text, x10 y82 w192 h16 +0x200 +Center, Toolbar skin:
Gui, 2:Add, Picture, x10 y114 w%skinSelW% h20 AltSubmit 0xE hwndhSelect gselSkin,
Gui, 2:Add, Button, x10 y140 w90 h24, OK
Gui, 2:Add, Button, x112 y140 w90 h24, Cancel
Gui, 2:Font, s7, Wingdings
Gui, 2:Add, Text, x10 y104 w%tscbw% h10 0x200 Center BackgroundTrans, �
Gui, 2:Font
; Generated using SmartGuiXP Creator mod 4.3.29.7
Gui, 2:Show, Hide, %appname% options
WinGet, hSet, ID, %appname% options
if A_IsCompiled
	{
	if !hSetClr := DllCall("LoadImage" AW
				, Ptr		, hInst
				, "UInt"	, skinres
				, "UInt"	, 0			; IMAGE_BITMAP
				, "Int"	, skinSelW	; cxDesired (width)
				, "Int"	, 20			; cyDesired (height)
				, "UInt"	, 0x2000)		; fuLoad
		ERR := DllCall("GetLastError", "UInt")
	}
else hSetClr := DllCall("LoadImage" AW
				, Ptr		, NULL
				, "Str"	, defskin
				, "UInt"	, 0
				, "Int"	, skinSelW
				, "Int"	, 20
				, "UInt"	, 0x2010)
if hP := DllCall("SendMessage" AW
			, Ptr		, hSelect
			, "UInt"	, 0x172		; STM_SETIMAGE
			, "UInt"	, 0			; IMAGE_BITMAP
			, Ptr		, hSetClr)
	DllCall("DeleteObject", Ptr, hP)
; select current values before showing the window
GuiControl, 2:Choose, swv1, %pref_isize%
GuiControl, 2:ChooseString, swv2, %lvsv%
GuiControl, 2:ChooseString, swv3, %lvlv%
swv4 := cSkin
ControlGetPos, cX,,cW,, Static5, ahk_id %hSet%
ControlMove, Static6, % cX+tscbw*(swv4-1),,,, ahk_id %hSet%
Gui, 2:Show, Center w209 h174, LLB Settings
OnMessage(0x200, "msmove")	; WM_MOUSEMOVE
if ERR
	msgbox, error=%ERR%
return
;================================================================
2ButtonOK:
;================================================================
Gui, 2:Submit
Gui, 1:Default
;Gui, 1:+Disabled
if (lvsv != swv2 OR lvlv != swv3)					; new list avatar size
	{
	GuiControl, -Redraw, LV1
	lvsv := swv2
	lvlv := swv3
	cap := avdir "\0." iExt
	gosub buildAvIL
	gosub m1_9a
	IL_Destroy(o1)
	IL_Destroy(o2)
	GuiControl, +Redraw, LV1
	gosub fixview
	}
LV_ModifyCol(1, "AutoHdr Sort Logical")
if (pref_isize != swv1)
	{
	pref_isize := swv1		; new toolbar icon size
	TB_SetIL(hTB1, "I0|" hILt%pref_isize%, 5)
	TB_SetIL(hTB2, "I0|" hILt%pref_isize%, 5)
	gosub tbrbset
;=== DELETE OLD MENU ICONS ===
	Loop, %rbidx%
		DllCall("DestroyIcon", Ptr, hIcon%A_Index%)
	Loop, Parse, midb, CSV
		{
		StringSplit, mc, A_LoopField, |
		StringSplit, ii, mc2, %A_Space%
		Loop, %ii0%
			DllCall("DestroyIcon", Ptr, hIM%mc1%_%mc2%_%A_Index%)
		}
	Loop, Parse, tmidb, CSV
		DllCall("DestroyIcon", Ptr, hIT%A_Index%)
;=== ADD NEW MENU ICONS ===
	Loop, Parse, blist, `n, `r
		{
		i := A_Index
		StringSplit, b, A_LoopField, |
		bpath%i% := b3 "\" b2
		hIcon%i% := GetIcon(bpath%i%, pref_isize="3" ? 2 : 3)	; 2=large icon h, 3=small icon h
		}
	Loop, 3
		{
		if !addicon(hm2_15, 3*A_Index-2, hIcon%bdef%)
			msgbox, addicon() error %ErrorLevel% for hm2_15 - %A_Index%
		else
			{
			i := A_Index
			MI_SetMenuStyle(hBrlist%i%, 0x04000000)
			Loop, %rbidx%
				if !addicon(hBrlist%i%, A_Index, hIcon%A_Index%)
					msgbox, addicon() error %ErrorLevel% for hBrlist%i% - %A_Index%
			}
		}
	Loop, Parse, midb, CSV
		{
		StringSplit, mc, A_LoopField, |
		StringSplit, ii, mc3, %A_Space%
		Loop, %ii0%
			{
			hIM%mc1%_%mc2%_%A_Index% := ILC_GetIcon(hILt%pref_isize%, ii%A_Index%)
			addicon(hm%mc1%_%mc2%, A_Index, hIM%mc1%_%mc2%_%A_Index%)
			}
		}
	Loop, Parse, tmidb, CSV
		{
		hIT%A_Index% := ILC_GetIcon(hILt%pref_isize%, A_LoopField)
		addicon(hmtray, A_Index, hIT%A_Index%)
		}
	}
if (cSkin != swv4)
	{
	cSkin := swv4			; new toolbar skin
	DllCall("DeleteObject", Ptr, hSkin)
	DllCall("DeleteObject", Ptr, hBack)
	hSkin := GetBmp(cSkin, skinsz, skin, 100, 0)
	skincolor := GetPixelColor(hSkin, 0, 1)
	menucolor := SwColor(skincolor)
	Menu, Tray, Color, %menucolor%
	Loop, Parse, clrMenus, CSV
		Menu, menu1_%A_LoopField%, Color, %menucolor%
	Loop, Parse, clrMenus2, CSV
		Menu, menu2_%A_LoopField%, Color, %menucolor%
	Menu, Tray, Color, %menucolor%
	ControlGetPos,,, w,,, ahk_id %hRB1%
	h=0
	r := RB_Get(hRB1, "r")
	c := RB_Get(hRB1, "c")
	Loop, %c%
		if ((nh%A_Index% := RB_GetBand(hRB1, A_Index, "hr")) > h)
			h := nh%A_Index%
	hBack := ResizeBmp(hSkin, w, h)
	RB_Set(hRB1, band1, "bk" hBack)
	RB_Set(hRB1, band2, "bk" hBack)
	tSkin=1		; if user changed skin, assume they want a fixed one so disable random skin choosing
	}
Gui, 1:-Disabled
;================================================================
2GuiClose:
2ButtonCancel:
OnMessage(0x200, "")
Gui, 2:Destroy
Gui, 1:Default
LV_Modify(oldrow, "-Select")
LV_Modify(oldrow, "Vis Focus Select")
return
;================================================================
selSkin:
;================================================================
MouseGetPos, mx, my, winid, ctrl, 2
ControlGetPos, cX, cY,,, Static5, ahk_id %hSet%
swv4 := 1+(mx-cX)//tscbw
return
;================================================================
resetsb:
SB_SetText("Ready")
SB_SetText(LV_GetCount() " entries", 2)
SB_SetText(me " marked", 3)
SB_SetText(dbfile, 5)
return
;================================================================
m1_18:			; ABOUT
;================================================================
MsgBox, 0x42040, About %appname%,
(LTrim
%appname% %version% (%releaseT%) released %releaseD%
%comment%.

Uses MI.ahk (MenuIcons script) by Lexikos
and FileGetVersionInfo() function by SKAN

Icons kindly provided by Mark James at:
http://www.famfamfam.com/lab/icons/silk/

Intended usage is for blog posts where the author wants to list
their favorite blogs or something similar. NOT for spamming !!!

The list of e-mails can be used in sending or forwarding messages.
Again: NOT for spamming !!!

I'm not responsible for the use or misuse of this application.
Please exercise common-sense when using it!

%author% , %releaseD%
)
return
;================================================================
;	FUNCTIONS
;================================================================
FileGetVersionInfo(peFile="", StringFileInfo="")	; function by SKAN (thank you)
;================================================================
{
Global AStr, WStr, Ptr, PtrP, AW, w9x
FSz := DllCall("Version\GetFileVersionInfoSize" AW, "Str", peFile, "UInt", 0)
IfLess, FSz,1, Return -1
VarSetCapacity(FVI, FSz, 0), VarSetCapacity(Trans, 8*2**(A_IsUnicode=TRUE), 0)
DllCall("Version\GetFileVersionInfo" AW, "Str", peFile, "Int", 0, "UInt", FSz, Ptr, &FVI)
If ! DllCall("Version\VerQueryValue" AW, Ptr, &FVI, "Str", "\VarFileInfo\Translation", PtrP, Translation, "UInt", 0)
	Return -2
f := (w9x OR !A_IsUnicode) ? "msvcrt\sprintf" : "msvcrt\swprintf"
If ! DllCall(f, "Str", Trans, "Str", "%08X", Ptr, NumGet(Translation+0, 0, Ptr), "CDecl") ; cdecl added by Drugwash
	Return -4
i := SubStr(Trans,-3)
if StringFileInfo = Lang
	{
	j := GetLocaleInfo("0x" i, 0x2)
	return (i = "0000" ? "Neutral" : j < 0 ? "0x" i : j)
	}
if StringFileInfo = LangN
	return "0x" i
subBlock := "\StringFileInfo\" i SubStr(Trans,1,4) "\" StringFileInfo
If ! DllCall("Version\VerQueryValue" AW, Ptr, &FVI, "Str", subBlock, PtrP, InfoPtr, "UInt", 0)
	Return
VarSetCapacity(Info, DllCall( "lstrlen" AW, Ptr, InfoPtr )*2**(A_IsUnicode=TRUE))
DllCall("lstrcpy" AW, "Str", Info, Ptr, InfoPtr)
Return Info
}
;================================================================
GetLocaleInfo(LCID=0x800, type=0x1)
;================================================================
{
; LOCALE_SYSTEM_DEFAULT=MAKELCID(MAKELANGID(0, 2), 0) 0x800
; LOCALE_USER_DEFAULT=MAKELCID(MAKELANGID(0, 1), 0) 0x400
; LOCALE_IDEFAULTLANGUAGE=0x9, LOCALE_ILANGUAGE=0x1
if !sz := DllCall("GetLocaleInfoA", "UInt", LCID, "UInt", type, "UInt", &data, "UInt", sz)
	{
;	msgbox, % "Error " DllCall("GetLastError", "UInt") " in " A_ThisFunc "`nLCID=" LCID " , type=" type
	return -1
	}
VarSetCapacity(data, sz+1, 0)
if !DllCall("GetLocaleInfoA", "UInt", LCID, "UInt", type, "UInt", &data, "UInt", sz)
	return -1
VarSetCapacity(data, -1)
return data
}
;================================================================
GetIGC(lib)		; GET ICONGROUP COUNT
;================================================================
{
Global Ptr
VarSetCapacity(buf, 4, 0)
CB := RegisterCallback("ERNP", "F", 4, &buf) 	; this is 'fast' version
inmem := DllCall("GetModuleHandle", "Str", lib)
resPath := (inmem) ? inmem : DllCall("LoadLibraryEx", "Str", lib, "UInt", 0, "UInt", 0x2, Ptr)
; RT_GROUP_ICON (11) + RT_ICON (3)
if resPath
	DllCall("EnumResourceNames", Ptr, resPath, "UInt", 14, Ptr, CB, "UInt", 0)
if !inmem
	DllCall("FreeLibrary", Ptr, resPath)
DllCall("GlobalFree", Ptr, CB)
return NumGet(buf, 0, "UInt")
}
;================================================================
ERNP(hModule, Rtype, RName, lP) ; EnumResNameProc callback
;================================================================
{
if r := A_EventInfo
	NumPut(NumGet(r+0, 0, "UInt")+1, r+0, 0, "UInt")
else msgbox, no buffer address
Return true
}
;================================================================
GetIcon(File="", h="0")
;================================================================
{
Static
Global AW, Ptr
if !init	; one-time switch to initialize COM and locally-defined types for AHK Basic
	{
	init := !DllCall("ole32\CoInitialize", "UInt", 0)	; SHGetFileInfo() requires prior COM init in AHK Basic
;	SHGFI_SYSICONINDEX := 0x4000
;	SHGFI_ICON := 0x100
;	SHGFI_USEFILEATTRIBUTES := 0x10
;	SHGFI_SHELLICONSIZE := 0x4
	_SHFILEINFOsz := VarSetCapacity(_SHFILEINFO, 352+340*(A_IsUnicode=TRUE)+4*(A_PtrSize=8), 0)
	addr := (A_PtrSize ? A_PtrSize : "4")
	}
else if (!File && init)
	return DllCall("ole32\CoUninitialize")
if !hIconList
	return hIconList := DllCall("shell32\SHGetFileInfo" AW
			 , "Str", File
			 , "UInt", 0x80
			 , Ptr, &_SHFILEINFO
			 , "UInt", _SHFILEINFOsz
			 , "UInt", 0x4010+h
			, Ptr)

DllCall("shell32\SHGetFileInfo" AW
		 , "Str", File
		 , "UInt", 0x80
		 , Ptr, &_SHFILEINFO
		 , "UInt", _SHFILEINFOsz
		 , "UInt", (h>"1" ? 0x104+h-2 : 0x4000+h))
; h=0 large icon, h=1 small icon, h=2=large handle, h=3 small handle
if h > 1
	return NumGet(_SHFILEINFO, 0, Ptr)			; return handle to icon (has overlay)
else
	return 1+NumGet(_SHFILEINFO, addr, "UInt")	; ImageList index is zero-based so bump it up one notch
}
;================================================================
addicon(hM, pos, hI)
;================================================================
{
VarSetCapacity(MII, 48, 0)		; MENUITEMINFO struct
NumPut(48, MII, 0, "UInt")		; cbSize
NumPut(0xA0,MII,4, "UInt")		; fMask MIIM_BITMAP|MIIM_DATA
NumPut(hI, MII, 32, "UInt")		; dwItemData
NumPut(-1, MII, 44, "Int")		; hbmpItem HBMMENU_CALLBACK
return DllCall("SetMenuItemInfo"
	, "UInt", hM				; hMenu
	, "UInt", pos-1				; uItem
	, "UInt", 1				; fByPosition
	, "UInt", &MII)
}
;================================================================
SwColor(res)
;================================================================
{
ofi := A_FormatInteger
SetFormat, Integer, Hex
if !InStr(res, "0x")
	{
	res := "0x" res
	nopre := TRUE
	}
r := ((res & 0xFF0000) >> 16) + (res & 0xFF00) + ((res & 0xFF) << 16)
if nopre
	StringTrimLeft, r, r, 2
SetFormat, Integer, %ofi%
return r
}
;================================================================
tmshow(wP, lP)			; adapted from the example script for MI.ahk by Lexikos
;================================================================
{
Global
if (lP = 0x205) 			; WM_RBUTTONUP
	{
	MI_ShowMenu(hmtray)	; Show menu to allow owner-drawing
	return 0
	}
}
;================================================================
cmnd(wP, lP, msg, hwnd)
;================================================================
{
Global
Local action, i
action := wP & 0xFFFF
action=%action%
if (lP=hTB1)
	{
	TBIdx := action
	SetTimer, action1, -5
	}
else if (lP=hTB2)
	{
	TB2Idx := action
	SetTimer, action2, -5
	}
}
;================================================================
notif(wP, lP, msg, hwnd2)
;================================================================
{
Global
Static hwnd, code, rect, w1, h1, coord="x1,y1,x2,y2"
SetFormat, Integer, H
hwnd := NumGet(lP+0, 0, Ptr)
code := NumGet(lP+0, 8, "UInt")
SetFormat, Integer, D
cmdid := NumGet(lP+0, 12, "UInt")
if (hwnd=hTB1)
	if code=0xFFFFFD3D			; TBN_QUERYDELETE
		{
		if (hwnd=hTB1)
			{
			}
		return TRUE
		}
	else if code=0xFFFFFD3E		; TBN_QUERYINSERT
		{
		if (hwnd=hTB1)
			{
			}
		return TRUE
		}
	else if (code=0xFFFFFD3A OR code=0xFFFFFFEC)		; TBN_DROPDOWN
		{
		SetTimer, menus1, -5
		}
	else if code=0xFFFFFD37		; TBN_HOTITEMCHANGE
		{
		}
if (hwnd=hTB2)
	if (code=0xFFFFFD3A OR code=0xFFFFFFEC)		; TBN_DROPDOWN
		{
		SetTimer, menus2, -5
		}
if (hwnd=hRB1)
	{
	 if code=0xFFFFFCBE		; RBN_AUTOSIZE
		{
		}
	else if code=0xFFFFFCB7	; RBN_CHEVRONPUSHED
		{
		msgbox, Chevron command #%cmdid%
		}
;	else if code=0xFFFFFCB9	; RBN_CHILDSIZE
;	else if code=0xFFFFFCBC	; RBN_ENDDRAG
	else if code=0xFFFFFCC1	; RBN_HEIGHTCHANGE
		{
		VarSetCapacity(rect, 16, 0)
		DllCall("GetClientRect", Ptr, hMain, Ptr, &rect)
		Loop, Parse, coord, CSV
			%A_LoopField% := NumGet(rect, 4*(A_Index-1), "Int")
		w1 := x2-x1, h1 := y2-y1
		SendMessage, 0x5, 0, w1|(h1<<16),, ahk_id %hMain%
		}
	}
}
;================================================================
msmove(wP, lP, msg, hwnd)
;================================================================
{
Critical
Global Ptr, hSet, hSelect, hCursH, tscbw, swv4
Static hide
MouseGetPos, mx, my, winid, ctrl, 2
if (winid != hSet)
	return
ControlGetPos, cX,,,, Static5, ahk_id %hSet%
if (ctrl != hSelect)
	{
	if !hide
		{
		ControlMove, Static6, % cX+tscbw*(swv4-1),,,, ahk_id %hSet%
		hide=TRUE
		}
	return
	}
ControlGetPos, X,,,, Static6, ahk_id %hSet%
s := (mx-cX)//tscbw
if (X != cX+tscbw*s)
	{
	ControlMove, Static6, % cX+tscbw*s,,,, ahk_id %hSet%
	GuiControl, 2:Show, Static6
	hide=
	}
;DllCall("SetCursor", Ptr, hCursH)
}
;================================================================
cpu(ByRef strg, pwd)
;================================================================
{
Global Ptr, AStr, appname, AW
;if !ucrypt := CryptIt(pwd, "SHA", FALSE)
if !ucrypt := CryptIt(pwd, "SHA", FALSE, A_IsUnicode)
	{
	MsgBox, 0x42010, %appname%, Can't hash the password!
	return
	}
sz := VarSetCapacity(strg)
VarSetCapacity(buf, 41, 0)
DllCall("RtlMoveMemory", Ptr, &buf, Ptr, &strg+sz-40, "UInt", 40)
VarSetCapacity(buf, -1)
if A_IsUnicode
	upass := DllCall("MulDiv", Ptr, &buf, "Int", 1, "Int", 1, AStr)
else upass := buf
if (ucrypt=upass)
	{
;	msgbox, password accepted
	VarSetCapacity(str2, sz+2, 0)
	DllCall("RtlMoveMemory", Ptr, &str2, Ptr, &strg, "UInt", sz-40)
	nsz := Crypto_RC4(&str2, sz-40, pwd, FALSE)
	if !A_IsUnicode
		{
		if !WC2MB(&str2, strg)
			MsgBox, 0x42010, %appname%, Cannot convert DB to ANSI!
		}
	else
		{
		r := VarSetCapacity(str2, -1)
		strg := str2
		}
	VarSetCapacity(str2, 0)
	return nsz
	}
MsgBox, 0x42010, %appname%, Wrong password! ;`n%ucrypt%`n%upass%
}
;================================================================
#include %A_ScriptDir%\lib
#include func_CryptDecryptRC4.ahk
#include func_CryptIt.ahk
#include func_EnumBrowsers.ahk
#include func_FileCreate.ahk
#include func_GetHwnd.ahk
#include func_GetOSver.ahk
#include func_IL_Replace.ahk
#include func_ImageList.ahk
#include func_OSTest.ahk
#include func_Rebar.ahk
#include func_Toolbar.ahk
#include func_UrlDownloadToVar.ahk
#include func_WC2MB.ahk
