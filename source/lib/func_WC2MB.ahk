; Conversion functions, Drugwash June 2015
; v1.0
;================================================================
WC2MB(pStrg, ByRef nt, cp=0)
;================================================================
{
Global Ptr
if !pStrg
	return
lib := A_IsUnicode ? "" : "unicows\"
if sz := DllCall(lib "WideCharToMultiByte"
			, "UInt", cp					; CP_UTF8 by default
			, "UInt", (cp="65001" ? 0 : 0x210)
			, Ptr, pStrg
			, "UInt", -1
			, Ptr, 0
			, "UInt", 0
			, "UInt", 0
			, "UInt", 0)
	{
	VarSetCapacity(nt, sz, 0)
	DllCall(lib "WideCharToMultiByte"
		, "UInt", cp						; CP_UTF8 by default
		, "UInt", (cp="65001" ? 0 : 0x210)
		, Ptr, pStrg
		, "UInt", -1
		, Ptr, &nt
		, "UInt", sz
		, "UInt", 0
		, "UInt", 0)
	VarSetCapacity(nt, -1)
	return sz
	}
else msgbox, % "error " DllCall("GetLastError") " in " A_ThisFunc "() - converting cp " cp " to ANSI, sz=" sz
VarSetCapacity(nt, 0)
}
;================================================================
MB2WC(strg, ByRef nt, cp=0)
;================================================================
{
Global Ptr
if (strg="")
	return
lib := A_IsUnicode ? "" : "unicows\"
if sz := DllCall(lib "MultiByteToWideChar"
			, "UInt", cp	; CP_ACP by default (UTF-8=65001, UTF-16=1200)
			, "UInt", 0
			, Ptr, &strg
			, "Int", -1
			, Ptr, 0
			, "UInt", 0)
	{
	sz*=2
	VarSetCapacity(nt, sz+1, 0)
	if !DllCall(lib "MultiByteToWideChar"
			, "UInt", cp	; CP_ACP by default
			, "UInt", 0
			, Ptr, &strg
			, "Int", -1
			, Ptr, &nt
			, "UInt", sz)
		{
		msgbox, % "error " DllCall("GetLastError") " in " A_ThisFunc "() - converting to CP" cp ", sz=" sz
		VarSetCapacity(nt, 0)
		}
	else
		{
		VarSetCapacity(nt, -1)
		return sz
		}
	}
else msgbox, % "error " DllCall("GetLastError") " in " A_ThisFunc "() - converting to CP" cp ", sz=" sz
}
