; Crypt/Decrypt file using RC4 (usable in Win9x where enhanced crypto is installed)
; based on MS example in MSDN and a script by shajul
; http://www.autohotkey.com/board/topic/21913-aes-encryptdecrypt-of-a-file-in-wxp-or-higher/page-2#entry388246
; � Drugwash, 24 June 2015, v1.2
;/*
;================================================================
;		EXAMPLE (uncomment block to run)
;================================================================
#NoEnv
#SingleInstance, Force
ListLines, Off
SetControlDelay, -1
SetWinDelay, -1
SetBatchLines, -1
ListLines, Off
StringCaseSense, On
SetFormat, Integer, D
DetectHiddenWindows, On
#include *i ..\updates.ahk		; optional, only for AHK Basic (ANSI)
;================================================================
sFile := "..\LLB.db"
pwd := "abcdefg1234"
w=RC4
SplitPath, sFile,,, fExt		; Retrieve original file's extension
fEnc := "Enc" w ".db"
fDec := "Dec" w "." fExt
e := FileCrypt_RC4(sFile, fEnc, pwd, TRUE)	; encrypt
d := FileCrypt_RC4(fEnc, fDec, pwd, FALSE)	; decrypt
return
;*/
;================================================================
FileCrypt_RC4(iFile, oFile, pass, enc=TRUE)
;================================================================
{
Global Ptr
if !iFile OR !oFile OR !FileExist(iFile)
	return
FileRead, viFile, *c %iFile%
if !viFile
	return
sz := VarSetCapacity(viFile)	; must check to make sure binary files report correct length!
r := VarSetCapacity(buf, sz+16*(enc=TRUE), 0)
DllCall("RtlMoveMemory", Ptr, &buf, Ptr, &viFile, "UInt", sz)
VarSetCapacity(viFile, 0)
nsz := Crypto_RC4(&buf, sz, pass, enc)
IfExist, %oFile%
	FileDelete, %oFile%
FileCreate(oFile, buf, 0, nsz)
VarSetCapacity(buf, 0)
return nsz
}
;================================================================
Crypto_RC4(pDt, sz, pwd, enc=TRUE)
;================================================================
{
Global AW, Ptr, PtrP, AStr
Static CALG_SHA1=0x8004, CALG_MD5=0x8003, CALG_RC4=0x6801
if (!sz OR r := DllCall("IsBadReadPtr", Ptr, pDt, "UInt", sz))
	{
	MsgBox, 0x2010, %A_ThisFunc%(), Error sz=%sz% or IsBadReadPtr()=%r%
	return 0
	}
len := StrLen(pwd)
if !A_IsUnicode
	MB2WC(pwd, sPass:= 0)	; This intermediary buffer is required
else sPass := pwd
SetFormat, Integer, H
API=CryptAcquireContextA	; Don't use W even on Unicode systems, it messes up!
if !DllCall("advapi32\" API
	, PtrP	, hProv
	, Ptr		, NULL
	, AStr	, "Microsoft Enhanced Cryptographic Provider v1.0"
	, "UInt"	, 1				; PROV_RSA_FULL
	, "UInt"	, 0)	; CRYPT_NEWKEYSET = 0x00000008
	{
	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
	return 0
	}
API=CryptCreateHash
if !DllCall("advapi32\" API
	, Ptr		, hProv
	, "UInt"	, CALG_SHA1	; can also use MD5
	, Ptr		, 0
	, "UInt"	, 0
	, PtrP	, hHash)
	{
	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
	goto Crypto_RC4_exit1
	}
API=CryptHashData
if !DllCall("advapi32\" API
	, Ptr		, hHash
	, Ptr		, &sPass
	, "UInt"	, len*2
	, "UInt"	, 0)
	{
	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
	goto Crypto_RC4_exit1
	}
API=CryptDeriveKey
if !DllCall("advapi32\" API
	, Ptr		, hProv
	, "UInt"	, CALG_RC4
	, Ptr		, hHash
	, "UInt"	, 128<<16
	, PtrP	, hKey)
	{
	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
	goto Crypto_RC4_exit2
	}
if sz <= 0x100000		; if file size under 1MB, use straight encryption code
	{
	API := enc ? "CryptEncrypt" : "CryptDecrypt"
	enc ? r1 := DllCall("advapi32\" API
			, Ptr		, hKey		; hKey
			, Ptr		, 0			; hHash
			, "UInt"	, TRUE		; Final
			, Ptr		, 0			; dwFlags
			, Ptr		, pDt		; pbData
			, PtrP	, sz			; pdwDataLen
			, "UInt"	, sz+16)		; dwBufLen
		: r1 := DllCall("advapi32\" API
			, Ptr		, hKey
			, Ptr		, 0
			, "UInt"	, TRUE
			, Ptr		, 0
			, Ptr		, pDt
			, PtrP	, sz)
	if !r1
		MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
	}
else	; this code will be used for very large files to avoid high memory usage
	{
	bLen=1024	; block length
	VarSetCapacity(buf, bLen*2)
	L := Ceil(sz/bLen)
	if enc
	Loop, %L%
		{
		i := A_Index<L ? bLen : sz-bLen*(A_Index-1)
		DllCall("RtlMoveMemory", Ptr, &buf, Ptr, pDt+bLen*(A_Index-1), "UInt", i)
		DllCall("advapi32\CryptEncrypt"
			, Ptr		, hKey			; hKey
			, Ptr		, 0				; hHash
			, "UInt"	, (A_Index=L)		; Final
			, Ptr		, 0				; dwFlags
			, Ptr		, &buf			; pbData
			, PtrP	, i				; pdwDataLen
			, "UInt"	, bLen*2)			; dwBufLen
		DllCall("RtlMoveMemory", Ptr, pDt+bLen*(A_Index-1), Ptr, &buf, "UInt", i)
		}
	else Loop, %L%
		{
		i := A_Index<L ? bLen : sz-bLen*(A_Index-1)
		DllCall("RtlMoveMemory", Ptr, &buf, Ptr, pDt+bLen*(A_Index-1), "UInt", i)
		DllCall("advapi32\CryptDecrypt"
			, Ptr		, hKey			; hKey
			, Ptr		, 0				; hHash
			, "UInt"	, (A_Index=L)		; Final
			, Ptr		, 0				; dwFlags
			, Ptr		, &buf			; pbData
			, PtrP	, i)				; pdwDataLen
		DllCall("RtlMoveMemory", Ptr, pDt+bLen*(A_Index-1), Ptr, &buf, "UInt", i)
		}
	}
Crypto_RC4_exit2:
API=CryptDestroyHash
if !DllCall("advapi32\" API, Ptr, hHash)
	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
API=CryptDestroyKey
if !DllCall("advapi32\" API, Ptr, hKey)
	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
Crypto_RC4_exit1:
API=CryptReleaseContext
if hProv
	DllCall("advapi32\" API, Ptr, hProv, "UInt", 0)
else 	MsgBox, 0x2010, %A_ThisFunc%(), % API "() Error " DllCall("GetLastError", "UInt")
SetFormat, Integer, D
Return sz+0
}
;================================================================
#include func_FileCreate.ahk
#include func_WC2MB.ahk
