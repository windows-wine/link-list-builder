; � Drugwash May 2014
; requires updates.ahk
 GetOSver(nice="")
{
Global Ptr, AW
Static suite="Small Business Server,Enterprise,BackOffice,,Terminal Services,Small Business Server (restricted),Embedded,DataCenter,Remote Desktop (single user),Home,Web Edition,,,Storage Server,Compute Cluster Edition"
off := 128*2**(A_IsUnicode=TRUE)
sSize := 28+off
VarSetCapacity(OVIX, sSize, 0)	; OSVERSIONINFOEX struct
NumPut(sSize, OVIX, 0, "UInt")
if !res := DllCall("GetVersionEx" AW, Ptr, &OVIX, "UInt")
	return
d2 := NumGet(OVIX, 4, "UInt")
d3 := NumGet(OVIX, 8, "UInt")
if !nice
	return d2 "." d3
d4 := NumGet(OVIX, 12, "UInt") & 0xFFFF
VarSetCapacity(d6, off, 0)
DllCall("lstrcpyn" AW, UInt, &d6, UInt, &OVIX + 20, UInt, 128)
VarSetCapacity(d6, -1)
d9a := NumGet(OVIX, 24+off, "UShort")
d10a := NumGet(OVIX, 26+off, "UChar")
d9=
Loop, Parse, suite, CSV
	if (d9a & 2**(A_Index-1))
		d9 .= A_LoopField " + "
StringTrimRight, d9, d9, 3
d10 := d10a=1 ? "Workstation" : d10a=2 ? "Domain controller" : d10a=3 ? "Server" : ""
return d2 "." d3 "." d4 " " d9 " " d10
}
