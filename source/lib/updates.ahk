; Data types and variables missing from AHK Basic
; � Drugwash 2012-2016	v.1.3
;================================================================
updates()
;================================================================
{
Global
Local i
w9x := A_OSType="WIN32_NT" ? FALSE : TRUE	; TRUE if system is Windows 95/98/98SE/ME
if isBasic := A_AhkVersion < "1.1" ? TRUE : FALSE	; TRUE if AHK version is Basic (not of AHK_L family)
	{
	Ptr := "UInt"							; Cast pointer type to unsigned integer
	UPtr := "UInt"							; Cast unsigned pointer type to unsigned integer
	PtrP := "UIntP"						; Cast pointer-to-pointer type to pointer-to-uinteger
	UPtrP := "UIntP"						; Cast pointer-to-upointer type to pointer-to-uinteger
	AStr := "Str"							; Cast ANSI string type to string
	WStr := "UInt"							; Pointer to buffer of Unicode-converted string
	i := "A_PtrSize"
	if !%i%
		%i% := "4"						; Set pointer size to DWORD (32bit)
	}
PtrSz := A_PtrSize							; Needed in functions since A_PtrSize is undeclarable in _L
i := "A_CharSize"
if !%i%
	%i% := 2**(A_IsUnicode=TRUE)			; Character size for use in structures and strings
i := "AW"
if !%i%
	%i% := A_IsUnicode ? "W" : "A"			; Helper for DllCall() ANSI/Unicode API versions
VarSetCapacity(i, 0)
return TRUE
}

/*
updates()
msgbox,
(
AHK version = %A_AhkVersion%
OS version  = %A_OSVersion%

w9x`t  = %w9x%
isBasic`t  = %isBasic%
Ptr`t  = %Ptr%
PtrP`t  = %PtrP%
AStr`t  = %AStr%
WStr`t  = %WStr%
A_PtrSize`t  = %A_PtrSize%
A_CharSize = %A_CharSize%
AW`t  = %AW%
)
*/
